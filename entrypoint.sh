#!/bin/sh
set -e

# apply default config
rsync --recursive --update --perms --owner --group --ignore-existing /etc/suricata.bak/ /etc/suricata

echo "sleeping for 10 sec"
sleep 10

/usr/sbin/crond

exec "$@"
